
<?php


use App\Http\Controllers\categorieController;
use App\Http\Controllers\produitController;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PanierController;
use App\Http\Controllers\cartController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Controllers\TypeaheadController;

Route::get('/', [AuthController::class, 'index'])->name('login');
Route::get('/resources/views/auth/register.blade.php', [AuthController::class, 'registration'])->name('registration');
Route::get('/resources/views/Projet1.blade.php', [AuthController::class, 'Projet1'])->name('Projet1'); 
Route::get('signout', [AuthController::class, 'signOut'])->name('signout');


// Admin


Route::prefix('admin')->middleware('auth')->group(function(){

  

});

Route::controller(categorieController::class)->group(function () {
    Route::get('/projet1', function () { return view('Projet1'); })->name('Projet1');

     Route::get('/index', 'index');
    Route::get('/categorie/create', 'create');
    Route::get('/categorie/{id}', 'show');
    Route::get('/categorie/{id}/edit', 'edit');

    Route::post('/categorie', 'store');
    Route::patch('/categorie/{id}', 'update');
    Route::delete('/categorie/{id}', 'destroy');

});

Route::controller(produitController::class)->group(function () {
    Route::get('/produit', 'produit');
    Route::get('/indice', 'indice');
    Route::get('/produit/creer', 'creer');
    Route::get('/produit/{id}', 'afficher');
    Route::get('/detaille/{id}', 'detaille');
    Route::get('/produit/{id}/modifier', 'modifier');

    Route::post('/produit', 'store');
    Route::patch('/produit/{id}', 'update');
    Route::delete('/produit/{id}', 'destroy');

});






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('projet1', function () { return view('projet1'); })->name('projet1');

Route::get('/produit', [TypeaheadController::class, 'index']);
Route::get('/autocomplete-search', [TypeaheadController::class, 'autocompleteSearch']);
// Route::get('/moustapha',function(){
//     return view('myform');})->name('myform') ;

// Route::name('produit.show')->get('produit/{produit}', 'ProduitController');

    Route::get('/produit', [ProduitController::class,'produit'])->name('produit.produits');
    Route::get('/allCategory', [ProduitController::class,'viewByCategorie'])->name('produit.parfum');
    Route::get('/makeUp', [ProduitController::class,' makeUp'])->name('produit.parfum');
    Route::get('/detaille', [ProduitController::class,'detaille'])->name('produit.detaille');
    // // Route::name('produit.show')->get('produit/{produits}', 'ProduitController');

    // Route::get("/panier", [ App\Http\Controllers\CartController::class,'store'])->name('panier.store');
    Route::post('add/cart', [CartController::class,'store']);

    Route::controller(CartController::class)->group(function () {

        Route::get('/monPanier', 'index')->name('mon-panier');
    
        Route::get('/panier/{id}', 'show');
    
    
        Route::get('/panier/create', 'create');
    
    
        Route::post('/panier', 'store');
        Route::delete('/panier/{id}', 'destroy');
       
    });
    // Route::post("/Panier", [ App\Http\Controllers\CartController::class,'store'])->name('panier.update');
    // Route::post("/Panier", [ App\Http\Controllers\CartController::class,'store'])->name('panier.destroy');
    // Route::post("/Panier", [ App\Http\Controllers\CartController::class,'store'])->name('panier.store');
    // Route::get('/panier', [ App\Http\Controllers\CartController::class,'cart'])->name('cart.panier');

    
    
    // Route::resource('panier', 'CartController')->only(['index', 'store', 'update', 'destroy']);
    // Route::controller(PanierController::class)->group(function () {

    //     Route::get('/panier', 'indexp');
    //     Route::get('/panier/create', 'create');
    //     Route::get('/panier/{id}', 'show');
    //     Route::get('/panier/{id}/edit', 'edit');
    
    
    //     Route::post('/panier', 'store');
    //     Route::patch('/panier/{id}', 'update');
    //     Route::delete('/panier/{id}', 'destroy');
    
    // });
    
    