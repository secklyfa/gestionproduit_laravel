<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Panier extends Model
{
    use HasFactory;
    protected $fillable = ['produit_id','nomProduit','quantite','prix','image'];

    public function produits(){
        return $this->belongsTo(Produit::class);
    }
}
