<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $fillable = ['produit_id','quantite'];

    public function produit(){
        return $this->belongsTo(Produit::class);
    }

    // static function add(array $table){
    //     var_dump($table);
    // }
}
