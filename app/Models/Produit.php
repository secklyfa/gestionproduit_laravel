<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    protected $fillable = ['id','image','nomProduit','prix','quantite','description','category_id'];

    public function category(){
        return $this->belongsTo(Categorie::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    // public static function getProduitStock($produit_id){
        
    //     $getProduitStock = Produit::Select('stock')-> where(['produit_id'=>$produit_id])->
    //     first();
    //     return $getProduitStock->stock;

    // }

}
