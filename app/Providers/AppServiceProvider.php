<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // {
        //     View::composer(['layouts.app1', 'produit.produits'], function ($view) {
        //         $view->with([
        //             'cartCount' => Cart::getTotalQuantity(), 
        //             'cartTotal' => Cart::getTotal(),
        //         ]);
        //     });
        // }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
  
}
