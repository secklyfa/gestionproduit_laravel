<?php

namespace App\Repositories;

use App\Models\Product as ModelsProduct;
use App\Models\Produit;
use App\Product;

interface BasketInterfaceRepository {

	// Afficher le panier
	public function show();

	// Ajouter un produit au panier
	public function add(ModelsProduct $produit, $quantite);

	// Retirer un produit du panier
	public function remove(ModelsProduct $produit);

	// Vider le panier
	public function empty();

}

?>