<?php

namespace App\Http\Controllers;
use App\Models\Produit;
use App\Models\categorie;
use App\Models\Cart;


use Illuminate\Http\Request;

class ProduitController extends Controller
{
   

    // public function __invoke(Request $request, Produit $produit)
    // {
    //     if($produit->active ) {
    //         return view('produit.show', compact('produit'));
    //     }
    //     return redirect(route('projet1'));
    // }

    
    
    public function categories()
    {

        $categories = categorie::all();
        return view('produit.produits', compact('categories', $categories));

    }
    public function produit()
    {
        //Pour afficher le nbre de produit dans le panier//
        $paniers = Cart::all();
        $numberOfItems =$paniers->count();
        
        $produits =Produit::all();
        $categories = categorie::all();
        

      

// pour filtre par categorie//
        if(\request($key = 'categorie')){
            $produits =Produit::where('category_id', request(key:'categorie'))->get();
            

            return view('produit.produits', compact('produits', 'categories','numberOfItems'));

        }
        
        // $produits = Produit::groupBy('category_id')->get();
        // $produits = Produit::where('category_id',3)->get();
        // $produits = Produit::where('category_id',4)->get();
        // $produits = Produit::orderBy('category_id', 'asc')->get();
        return view('produit.produits', compact('produits', 'categories','numberOfItems'));

    }

   

   
    


    //  /**
    //  * Affiche la liste des produits
    //  */
    public function indice()
    {

        $produits =Produit::all();
        return view('produit.indice', compact('produits'));

    }


    /**
     * return le formulaire de créationcreation d'un produit
     */
    public function creer()
    {
        $categories = categorie::all();
        return view('produit.creer', compact('categories'));

    }


    // /**
    //  * Enregistre un nouveau produit dans la base de données
    //  */
    public function store(Request $request)
    {

        $request->validate([
            'nomProduit'=>'required|between:5,20|alpha',
            'prix'=>'required|between:1000,10000|int',
            'quantite'=>'required|between:1,2|int',
            'description'=>'required|between:10,50',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
            
           
        ]);

        $image = $request->file('image');
        $NomImage= $image->getClientOriginalName();
        $image->storeAs('/public/img',$NomImage );



        $produit = new Produit([
            'category_id' => $request->get('category_id'),
            'nomProduit' => $request->get('nomProduit'),
            'prix' => $request->get('prix'),
            'description' => $request->get('description'),
            'quantite' => $request->get('quantite'),
            'image' => $NomImage,
            
           
        ]);


        $produit->save();
        return redirect('/projet1')->with('success', 'produit Ajouté avec succès');

    }


    // /**
    //  * Affiche les détails d'un produit spécifique
    //  */

    public function afficher($id)
    {

        $produit = Produit::findOrFail($id);
        return view('produit.afficher', compact('produit'));

    }


    // /**
    //  * Return le formulaire de modification
    //  */

    public function modifier($id)
    {

        $produit = Produit::findOrFail($id);

        return view('produit.modifier', compact('produit'));

    }


    // /**
    //  * Enregistre la modification dans la base de données
    //  */
    public function update(Request $request, $id)
    {

        $request->validate([

            'nomProduit'=>'required',
            'prix'=>'required',
            'quantite'=>'required',
            'category_id'=>'required',

        ]);




        $produit = produit::findOrFail($id);
        $produit->nomProduit = $request->get('nomProduit');
        $produit->prix = $request->get('prix');
        $produit->quantite = $request->get('quantite');
        $produit->category_id = $request->get('category_id');
       


        $produit->update();

        return redirect('/')->with('success', 'produit Modifié avec succès');

    }



    // /**
    //  * Supprime le produit dans la base de données
    //  */
    public function destroy($id)
    {

        $produit = produit::findOrFail($id);
        $produit->delete();

        return redirect('/projet1')->with('success', 'produit Supprime avec succès');

    }

    public function detaille($id)
    {
        $paniers = Cart::all();
        $numberOfItems =$paniers->count();

        $produit = Produit::findOrFail($id);
        return view('produit.detaille', compact('produit','paniers','numberOfItems'));

    }

    // public function viewByCategorie(){
    //     $produits = Produit::where('category_id',3)->get();
    //     // dd( $categories);

    //     return view('produit.parfum', compact('produits'));
    // }

   

   





// trie des produits par categorie//

// public function trier(){
//     $produit = Produit::where('category_id', 3);
//     return view('produit.produits', compact('produit'));
    

   
// }

 
}





   

