<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Models\Produit;
use App\Models\Cart;
use Session;

class CartController extends Controller
{
   

//     public function cart()
// {
//     $content = Produit::all();
//     $total = Cart::all();
//     return view('cart.panier', compact('content', 'total'));
// }


public function create()
    {
        $produit = Produit::all();

        return view('panier.create', compact('produit'));
    }


   
public function store(Request $request)
{
$panier= new Cart([
    'produit_id'=>$request->post('produit_id'),
    'quantite'=>$request->post('quantite'),
   
]);   
   $panier->save();   
    return redirect()->back()->with('success', 'Produit ajouté avec succesé');
}




public function index()
{
    $paniers = Cart::all();

    $numberOfItems =$paniers->count();

    return view('panier.index', compact('paniers','numberOfItems'));
}



public function show($id)
{
    $produit = Produit::findOrFail($id);
    return view('produit.afficher', compact('produit'));
    // $panier = Cart::findOrFail($id);
    // return view('panier.show', compact('panier'));
}

// public function update(Request $request, $id)
// {

//     $panier = Cart::findOrFail($id);
//     $panier->produit_id = $request->get('produit_id');
//     $panier->quantite = $request->get('quantite');
    

//     $panier->update();

//     return redirect('/')->with('success', 'panier Modifié avec succès');

// }
public function destroy($id)
{

    $panier = Cart::findOrFail($id);
    $panier->delete();

    return redirect('/monPanier')->with('success', 'produit Supprime avec succès');

}

// static function cartItem(){
    // $paniers=Session ::get('panier');
//     $paniers = Cart::all()->count();
//     return view('panier.index', compact('paniers'));
// }
public function nbproduit(){
        $panier = Cart::all()->count();
        return view('produit.produits', compact('produit'));
        
    
       
    }

}
