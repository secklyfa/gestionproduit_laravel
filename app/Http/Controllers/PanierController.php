<?php

namespace App\Http\Controllers;

use App\Models\Panier;
use App\Models\Produit;
use Illuminate\Http\Request;

class PanierController extends Controller
{
    
    public function index()
    {

        $paniers = Panier::all();
        // return view('panier.index', compact('paniers'));
        $produits = Produit::all();
        
        return view('cart.panier', compact('paniers', 'produits'));

       

    }


    /**
     * return le formulaire de créationcreation d'un contact
     */
    public function create()
    {

        return view('panier.create');

    }


    /**
     * Enregistre un nouveau contact dans la base de données
     */
    public function store(Request $request)
    {

       


        $panier = new panier([
            'nomProduit' => $request->get('nomProduit'),
            'prix' => $request->get('prix'),
            'quantite' => $request->get('quantite'),
           
        ]);


        $panier->save();
        return redirect('/')->with('success', 'Contact Ajouté avec succès');

    }


    /**
     * Affiche les détails d'un contact spécifique
     */

    public function show($id)
    {

        $panier = panier::findOrFail($id);
        return view('panier.show', compact('panier'));

    }


    /**
     * Return le formulaire de modification
     */

    public function edit($id)
    {

        $panier = panier::findOrFail($id);

        return view('panier.edit', compact('panier'));

    }


    /**
     * Enregistre la modification dans la base de données
     */
    public function update(Request $request, $id)
    {

      




        $panier = panier::findOrFail($id);
        $panier->nom = $request->get('nomProduit');
        $panier->email = $request->get('prix');
        $panier->telephone = $request->get('quantite');
       


        $panier->update();

        return redirect('/')->with('success', 'contact Modifié avec succès');

    }




    /**
     * Supprime le contact dans la base de données
     */
    public function destroy($id)
    {

        $panier = panier::findOrFail($id);
        $panier->delete();

        return redirect('/')->with('success', 'contact Supprime avec succès');

    }
}
