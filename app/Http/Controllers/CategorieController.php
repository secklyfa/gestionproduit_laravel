<?php


namespace App\Http\Controllers;


use App\Models\categorie;
use Illuminate\Http\Request;


class categorieController extends Controller
{

    public function categorie()
    {

        $categories = categorie::all();
        return view('produit.produits', compact('categories', $categories));

    }
    
    /**
     * Affiche la liste des categories
     */
    public function index()
    {

        $categories = categorie::all();
        return view('categorie.index', compact('categories'));

    }


    /**
     * return le formulaire de création d'un categorie
     */
    public function create()
    {

        return view('categorie.create');

    }


    /**
     * Enregistre un nouveau categorie dans la base de données
     */
    public function store(Request $request)
    {

        $request->validate([
            'nom'=>'required|between:5,20|alpha',
           
        ]);


        $categorie = new categorie([
            'nom' => $request->get('nom'),
           
        ]);


        $categorie->save();
        return redirect('/projet1')->with('success', 'categorie Ajouté avec succès');

    }


    /**
     * Affiche les détails d'un categorie spécifique
     */

    public function show($id)
    {

        $categorie = categorie::findOrFail($id);
        return view('categorie.show', compact('categorie'));

    }


    /**
     * Return le formulaire de modification
     */

    public function edit($id)
    {

        $categorie = categorie::findOrFail($id);

        return view('categorie.edit', compact('categorie'));

    }


    /**
     * Enregistre la modification dans la base de données
     */
    public function update(Request $request, $id)
    {

        $request->validate([

            'nom'=>'required',
           

        ]);




        $categorie = categorie::findOrFail($id);
        $categorie->nom = $request->get('nom');
       


        $categorie->update();

        return redirect('/projet1')->with('success', 'categorie Modifié avec succès');

    }




    /**
     * Supprime le categorie dans la base de données
     */
    public function destroy($id)
    {

        $categorie = categorie::findOrFail($id);
        $categorie->delete();

        return redirect('/projet1')->with('success', 'categorie Supprime avec succès');

    }

}