@extends('layouts.app')


@section('content')

    <h1>Ajouter un categorie</h1>
    <div class="justify-content-center d-flex">
<div class="card w-50">


    @if ($errors->any())

        <div class="alert alert-danger">

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach

            </ul>

        </div>

    @endif

    <form action="{{ url('categorie') }}" method="POST">
        @csrf

        <div class="form-group mb-3">
            <label for="nom">Nom Categorie:</label>
            <input type="text" class="form-control  @error('nom') is-invalid @enderror" name="nom" id="nom" placeholder="Votre nom" value="{{ old('nom') }}">
                        @error('nom')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
            {{-- <input type="text" class="form-control w-50" id="nom" placeholder="Entrez un nom" name="nom"> --}}
        </div>

      

        <button type="submit" class="btn btn-primary" style='background:#63C9E8'>Enregister</button>

    </form>
</div>
</div>
@endsection
