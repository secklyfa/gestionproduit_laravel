@extends('layouts.app')

@section('content')

    <div class="row mt-3">

        <div class="col-lg-11 mt-3">

            <h2>Gestion categorie</h2>

        </div>

        <div class="col-lg-1 mt-5">
            <a class="btn bg-gradient-success" href="{{ url('categorie/create') }}">Ajouter</a>
        </div>

    </div>



    @if ($message = Session::get('success'))

        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>

    @endif



    <table class="bg-light table table-bordered table-responsive mt-5 " style='text-align: center'>

        <tr>

            <th>No</th>
            <th>Nom Categorie</th>
           
            <th>Actions</th>

        </tr>

        @foreach ($categories as $index => $categorie)

            <tr>
                <td>{{ $index }}</td>
                <td>{{ $categorie->nom }}</td>
               
                <td>

                    <form action="{{ url('categorie/'. $categorie->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a class="btn btn-info" href="{{ url('categorie/'. $categorie->id) }}"><i class="fa fa-eye color-white" aria-hidden="true"></i></a>
                        <a class="btn btn-primary" href="{{ url('categorie/'. $categorie->id .'/edit') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>

                    </form>
                </td>

            </tr>

        @endforeach
    </table>

@endsection
