@extends('layouts.app')


@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-md-11">
            <div class="d-flex justify-content-between">
                <h2>Liste des produits dans le panier</h2>


            </div>


        </div>
    </div>
    <div class="row  justify-content-center mt-3">

        <div class="col-md-11">

            <div class="card">

                <div class="table-responsive">


                    <table class="table ">

                        <tr class="text-center" style="background:#0C0F0A;color:#fff">
                            <th>No</th>
                            <th>Image</th>

                            <th>Nom</th>
                            <th>Quantité</th>
                            <th>Prix</th>

                            <th>User_id</th>


                            <th>Actions</th>



                        </tr>
                        @foreach ($paniers as $index => $panier)
                            <tr class="text-center text-muted">
                                <td>
                                    <p>{{ $index }}</p>
                                </td>

                                <div class="col-lg-2 mb-3">
                                    <img src="{{ asset('storage/images/' . $panier->produit->image) }}"
                                        class="rounded-circle" alt="{{ $produit->name }}" style="width: 40px;height:40px">
                                </div>
                                <td>
                                    <p>{{ $panier->produits->nomProduit }}</p>
                                </td>

                                <td>
                                    <p>{{ $panier->quantite }}</p>
                                </td>


                                <td>
                                    <p>{{ $panier->produits->prix }} FCFA</p>
                                </td>


                                {{-- <td>
                                    <p>{{ $panier->user->id }}</p>

                                </td> --}}
                                <td>
                                    <form action="{{ url('Panier/' . $panier->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <a class="btn  rounded-circle  " href="{{ url('Panier/' . $panier->id) }} "
                                            style="background:#FE277E;color:#FFFFFF;width:40px;padding:8px ">
                                            <x-carbon-view style="width: 18px; height:18px" />
                                        </a>

                                        <button type="submit" class="btn  rounded-circle"
                                            style="background: #47EAD0;color:#FFFFFF;width:40px;padding:8px"">
                                            <x-ri-delete-bin-6-line style="width: 18px;height:18px" />
                                        </button>

                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>



                {{-- <td>{{ $panier->produit_id}}</td> --}}
                {{-- <td>{{ $panier->produit->category->nom }}</td> --}}







                <div>


                </div>
            </div>

        </div>
    </div>
    <div class="text-center">

        {{-- <a class="btn btn-success" href="{{ url('/login') }}">COMMANDER</a> --}}
    </div>

    </div>
    <div class=" mt-5 text-center">
        {{-- <h2>Votre panier est vide</h2>
                <a class="btn btn-success" href="{{ url('/') }}">Ajouter des produits</a> --}}

        {{-- <h5>Prix total: {{ $totalPrice }}</h5> --}}
        {{-- <a class="btn btn-success" href="{{ url('/login') }}">COMMANDER</a> --}}
    </div>
@endsection
