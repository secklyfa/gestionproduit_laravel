@extends('layouts.app1')


@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-md-11">
            <div class="d-flex justify-content-between">
                <h2>Liste des produits dans le panier</h2>


            </div>


        </div>
    </div>
    <div class="row  justify-content-center mt-3">

        <div class="col-md-11">

            <div class="card">

                <div class="table-responsive">


                    <table class="table ">

                        <tr class="text-center" style="background:#EF6499;color:#fff">
                            <th>No</th>
                            <th>Image</th>
                            <th>Nom</th>
                            <th>Quantité</th>
                            <th>Prix</th>
                            <th>Actions</th>
                        </tr>
                    

                        @foreach ($paniers as $index=> $panier)
                            <tr class="text-center text-muted">
                                <td>
                                    <p>{{ $index }}</p>
                                </td>
                                
                                <td>
                                    <img src="assets/img/{{ $panier->produit->image }}" style="width: 50px"
                                        class="image" alt="{{$panier->produit->nomProduit }}" >
                                </td>
                                <td>
                                    <p>{{ $panier->produit->nomProduit }}</p>
                                </td>

                                <td>
                                    <p>{{ $panier->quantite }}</p>
                                </td>

                               
                                <td>
                                    <p>{{ $panier->produit->prix }} FCFA</p>
                                </td>


                               
                                <td>
                                     <form action="{{ url('panier/' . $panier->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-info  " href="{{ url('panier/'. $panier->id) }}"><i class="fa fa-eye color-white" aria-hidden="true"></i></a>
                                      
                
                                        <button type="submit" class="btn btn-danger "><i class="fa fa-trash" aria-hidden="true"></i></button>

                                       
                                    </form> 
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>



             





                <div>


                </div>
            </div>

        </div>
    </div>
    <div class="text-center">

        {{-- <a class="btn btn-success" href="{{ url('/login') }}">COMMANDER</a> --}}
    </div>

    </div>
    <div class=" mt-5 text-center">
        {{-- <h2>Votre panier est vide</h2>
                <a class="btn btn-success" href="{{ url('/') }}">Ajouter des produits</a> --}}

        {{-- <h5>Prix total: {{ $totalPrice }}</h5> --}}
        {{-- <a class="btn btn-success" href="{{ url('/login') }}">COMMANDER</a> --}}
    </div>

    
@endsection
