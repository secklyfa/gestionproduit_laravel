{{-- <php?
use Cart;

php> --}}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        projet1 Laravel
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body class="g-sidenav-show  bg-gray-200">

    <main class="main-content position-relative max-height-vh-120 h-120 border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main w-100 fixed-top navbar-expand-lg px-0  shadow-none " style='background:#f183ad' id="navbarBlur"
            data-scroll="true">
            <div class="container-fluid py-1 px-3">
                <nav aria-label="breadcrumb">
                  <a href="/produit" style="text-decoration: none" >
                    <h3 class="font-weight-bolder mb-0" style='color:white'>MyShop</h3>
                  </a>
                </nav>
                {{-- <div classs="form-group ">
                    <input class="recherche m-3" type="text" id="search" name="search" placeholder="Search" class="form-control" />
                </div>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js">
    </script>
    <script type="text/javascript">
        var route = "{{ url('autocomplete-search') }}";
        $('#search').typeahead({
            source: function (query, process) {
                return $.get(route, {
                    query: query
                }, function (data) {
                    return process(data);
                });
            }
        });
    </script> --}}
                <div class="collapse navbar-collapse justify-content-end">

                    {{-- <a class="nav-link text-white " href="/login">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="fa fa-user" aria-hidden="true"style='color:white;font-size:20px'></i>
                        </div>
                        <span class="nav-link-text ms-1" style='color:white;font-size:18px'>login</span>
                    </a> --}}

                     <a class="nav-link text-white " href="/monPanier">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="fa fa-cart-arrow-down " aria-hidden="true" style='color:white;font-size:20px' >   ({{ $numberOfItems}}) </i>
                        </div>
                        <span class="nav-link-text ms-1" style='color:white;font-size:18px'> 
                        Panier
                     
                            
                        </span>
                    </a> 
                
                    {{-- <ul class="right hide-on-med-and-down">
                        @if($cartCount)
                          <li>
                            <a class="tooltipped" href="{{ route('cart.panier') }}" data-position="bottom" data-tooltip="Voir mon panier">
                                <i class="material-icons left">shopping_cart</i>Panier({{ $cartCount }})</a>
                          </li>
                        @endif
                       
                    </ul> --}}
                </div>
            </div>



                </div>

            </div>
        </nav>
        
        <!-- End Navbar -->
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>

            </div>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>


</html>
