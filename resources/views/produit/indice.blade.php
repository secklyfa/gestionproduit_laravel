@extends('layouts.app')

@section('content')

    <div class="row mt-5">

        <div class="col-lg-11">

            <h2>Gestion produit</h2>

        </div>

        <div class="col-lg-1">
            <a class="btn  bg-gradient-success" href="{{ url('produit/creer') }}">Ajouter</a>
        </div>

    </div>
  
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>

    @endif


    
    <table class=" table table-bordered mt-5 bg-light" style='text-align: center'>

        <tr>

            <th>Id</th>
            <th>Nom Produit</th>
            <th>Prix</th>
            <th>Quantite</th>
            <th>Nom Categorie</th>
            <th>Actions</th>
            {{-- <th>Description</th> --}}
            
           

        </tr>

        @foreach ($produits as $index => $produit)

            <tr>
                {{-- <td><img src="/images/{{ $produit->image }}" width="100px"></td> --}}
                {{-- <td> --}}
                                    
                    {{-- <img src="{{ asset('/assets/img' . $produit->image) }}" class="rounded-circle img-fluid" alt="{{ $produit->name }}"
                     style="width: 40px;height:40"></td> --}}
                 <td>{{ $index }}</td>     
                <td>{{ $produit->nomProduit }}</td>
                <td>{{ $produit->prix }}</td>
                <td>{{ $produit->quantite }}</td>
                {{-- <td>{{ $produit->description }}</td> --}}
                <td>{{ $produit->category->nom}}</td>
                <td>

                    <form action="{{ url('produit/'. $produit->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a class="btn btn-info" href="{{ url('produit/'. $produit->id) }}"><i class="fa fa-eye color-white" aria-hidden="true"></i></a>
                        <a class="btn btn-primary" href="{{ url('produit/'. $produit->id .'/modifier') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>

                    </form>
                </td>

            </tr>

        @endforeach
    </table>
</div>
@endsection
