@extends('layouts.app')


@section('content')


    <h1>Modifier produit</h1>


    @if ($errors->any())

        <div class="alert alert-danger">

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

        </div>

    @endif

    <form method="post" action="{{ url('produit/'. $produit->id) }}" >
        @method('PATCH')
        @csrf


        <div class="form-group mb-3">

            <label for="nomProduit">Nom Produit:</label>
            <input type="text" class="form-control" id="nomProduit" placeholder=" Nom" name="nomProduit" value="{{ $produit->nomProduit }}">

        </div>

        <div class="form-group mb-3">

            <label for="prix">Prix:</label>
            <input type="number" class="form-control" id="prix" placeholder=" prix" name="prix" value="{{ $produit->prix }}">

        </div>

       

        <div class="form-group mb-3">

            <label for="quantite">Quantite:</label>
            <input type="number" class="form-control" id="quantite" placeholder="Quantite" name="quantite" value="{{ $produit->quantite }}">

        </div>

        

        

        <button type="submit" class="btn btn-primary">Enregistrer</button>

    </form>

@endsection