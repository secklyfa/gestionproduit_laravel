@extends('layouts.app1')


@section('content')

    <h1>Detaille produit</h1>



    <div class="card-group" >
        <div class="card">
            <div class="card-image">
                <img src="assets/img/{{ $produit->image }}" class="produits w-100">        
                {{-- <img src="/public/img{{ $produit->image }}" alt="{{$produit->nomProduit }}" class="produits w-100"> --}}
                <h1 class="title mt-3"> {{ $produit->description }}</h1>
             
            </div> 
         
        </div>
        <div class="card">
          <div class="card-body">
          
          
          <div  >
          <h3 class="para mt-3">{{ $produit->nomProduit }}</h3>
             <h1 class="title mt-3"> {{ $produit->prix }}</h1>
      {{-- <p style={{fontSize:18}}>This solid cherry wood bench comes with a leather seat in white, black, and brown colors. The bench features American style and modern design.
       --}}
      </p>
      <form  method="POST" action="{{ url('add/cart') }}" id="panier_add">
        @csrf
        <div class="input-field col">
          
          <input type="hidden" id="produit_id" name="produit_id" value="{{ $produit->id }}">
          <input id="quantite" name="quantite" type="number" value="1" min="1">
          <label for="quantite">Quantité</label>        
          <p>
            <button class="btn btn-success waves-effect waves-light mt-5"  style="width:100%,;background:#EF6499" type="submit" >Ajouter au panier
              <i class="material-icons left">add_shopping_cart</i>
            </button>
          </p>    
        </div>    
      </form>
      </div>    
            {{-- <p>{{ $produit->quantite }}</p> --}}
          
            </div>
            
          </div>
        </div>
      </div>
  

@endsection
