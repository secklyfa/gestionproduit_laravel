@extends('layouts.app2')


@section('content')
<div class=' content-justify-center '>

    <h1>Ajouter un produit</h1>
</div>
<div class="justify-content-center d-flex">
<div class="card w-80 justify-content-center d-flex">


    @if ($errors->any())

        <div class="alert alert-danger">

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach

            </ul>

        </div>

    @endif

    <form class=' justify-content-center m-5' action="{{ url('produit') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <label class="block mb-4">
            <span class="sr-only">Choose File</span>
            <input type="file" name="image"
                class="block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100" />
            @error('image')
            <span class="text-red-600 text-sm">{{ $message }}</span>
            @enderror
        </label>

        <!-- <div class="form-group mb-3">
            <label for="nomProduit">Nom Produit:</label>
            <input type="text" class="nomProduit w-50" id="nomProduit" placeholder="Entrez un nom" name="nomProduit">
        </div> -->
        <div class="form-group mb-3 text-align-center">
          <label for="nomProduit">Nom Produit:</label>
          <input type="text" class="form-control w-70" id="nomProduit" placeholder="Nom Produit" name="nomProduit">
        </div>
        {{-- <input type="text" class="form-control  @error('nom') is-invalid @enderror" name="nom" id="nom" placeholder="Votre nom" value="{{ old('nom') }}">
        @error('nom')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror --}}
        <div class="form-group mb-3">

            <label for="prix">Prix:</label>
            <input type="number" class="form-control w-70" id="prix" placeholder="prix" name="prix">

        </div>


        <div class="form-group mb-3">
            <label for="quantite">Quantite:</label>
            <input type="number" class="form-control w-70" id="quantite" placeholder="quantite" name="quantite">
        </div>

        <div class="form-group mb-3">
            <label for="description">Description:</label>
            <input type="text" class="form-control w-70" id="description" placeholder="description" name="description">
        </div>
       
        <select class="form-select w-70" aria-label="Default select example" type='text' name="category_id">
        @foreach($categories as $categorie)
      
          <!-- <option >{{ $categorie->nomCategorie }}</option> -->
          <option  value= '{{ $categorie->id }}'>{{ $categorie->nom }}</option>

          @endforeach
         </select>

        <button type="submit" class="btn btn-primary mt-4" style='background:#63C9E8'>Enregister</button>

    </form>
    </div>
</div>
<div class="justify-content-center d-flex">
@endsection