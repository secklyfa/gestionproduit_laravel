@extends('layouts.app1')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="container ">
      
      <div class="row ">
        
        <div class="col-md-3 "> 
          <div class="categorie ">
            <h3 class='para justify-content-center d-flex' style='color:#f183ad' > Categories</h3>
        </div>
        <div class="card ">
         
          <div class="row  g-3  justifi-content-center d-flex">
            
          @forelse($categories as $categorie)
          
              @if($categorie->nom)
              {{-- <h5 style="text-decoration:none; margin:5px"></h5> --}}

                {{-- pour filtre par categorie --}}
                <a style="text-decoration: none" href="/produit?categorie={{($categorie->id)}}">
                   
                     <h5 style="text-decoration:none; margin:9px"> {{$categorie->nom}} </h5>
                </a>  
              </h4>
              @endif
             
              @empty
              <h3>pas de produit pour l'instant</h3>
        @endforelse

      
                </div>
            </div>
        </div>
         
            <div class="col-md-9 "> 
              <h3 class='para justify-content-center d-flex' style='color:#f183ad' > les Produits</h3>  
                <div class="card ">
              <div id="carouselExampleSlidesOnly" class="carousel slide " data-bs-ride="carousel">
                  <div class="carousel-inner  ">
                    <div class="carousel-item   active" >
                      <img src="https://media.istockphoto.com/id/624065268/fr/photo/sac-%C3%A0-main-femme-mode-avec-t%C3%A9l%C3%A9phone-portable-maquillage-et-accessoires.jpg?s=612x612&w=0&k=20&c=Mr9AOOAVfJqsTwMdw2xBRU3bm_33SI8TgdrTVMS1knI=" class="car w-80 h-50 " alt="..."  ">
                    </div>
                    
                    <div class="carousel-item ">
                      <img src="https://media.istockphoto.com/id/502559877/fr/photo/rouge-%C3%A0-l%C3%A8vres-gris-avec-des-fleurs-dorchid%C3%A9e.jpg?s=612x612&w=0&k=20&c=1SV6kiY6iAX0cTjPCwmKBrxDb7cKLU4Dmz8vukoSK-Q=" class="d-block w-80 " alt="...">
                    </div>
                    <div class="carousel-item ">
                      <img src="https://media.istockphoto.com/id/462131539/fr/photo/femmes-s-bijoux-produits-cosm%C3%A9tiques-et-parfums.jpg?s=612x612&w=0&k=20&c=8PaGpe3idilNfuW48oyxB7ko6Mdjl-RR_1polOdR7u0=" class="d-block w-80  " alt="...">
                    </div>
                    <div class="carousel-item ">
                      <img src=" https://media.istockphoto.com/id/607747890/fr/photo/parfum-avec-des-fleurs.jpg?s=612x612&w=0&k=20&c=vzHXDCE4CIuaQmjwbK52WvvtzYMMS3O954GTuxzcT5M=" class="d-block w-80 " alt="...">
                    </div>
                  </div>
                
                </div>
                {{-- <h3 class='para justify-content-center d-flex mt-5' style='color:#f183ad' >Tous les Produits</h3> --}}
              </div>
            </div>
           
              <div class="row  row-cols-md-4 g-2  justifi-content-center d-flex" id='lait_de_corps'>
              
                @foreach($produits as $produit)
                    <div class="card shadow-flex">
                      <div class="card-image">
                        
                          <img src="assets/img/{{ $produit->image }}" class="produits w-100">
                       
                      </div>          
                      <div class="card-content center-align">
                        <h6>{{ $produit->nomProduit }}</h6>
                        @if($produit->quantite)
                          <h5 ><strong>{{ number_format($produit->prix) }} FCFA </strong></h5>
                          <form  method="POST" action="{{url('/produit')}}">
                            @csrf
                            <div class="input-field col">
                              <input type="hidden" id="produit_id" name="produit_id" value="{{ $produit->id }}">
                              <a class="btn " style="background: #EF6499; color:white" href="{{ url('detaille/'. $produit->id) }}">VOIR</i></a>
                                  
                            </div>    
                          </form>
                         
                        @endif
                      </div>
                    </div>
               
                  @endforeach
               
              </div>
            </div>
      </div>
      
      </div>
    </div>
        
         
       
        
            

              {{-- <div class='lait_de_corps mt-5' >
                <h3 class='para justify-content-start d-flex' style='color:#EF6499' >LAIT DE CORPS</h3>
            </div> --}}
            {{-- <div class="card-group mt-5" id='lait_de_corps' >
                @foreach($produits as $produit)
                <div class="card m-3">
                  <div class="card-image">
                    @if($produit->quantite)
                      <a href="#">
                    @endif
                      <img src="assets/img/{{ $produit->image }}" class="produits w-100">
                    @if($produit->quantite) </a> @endif
                  </div>          
                  <div class="card-content center-align">
                    <p>{{ $produit->nomProduit }}</p>
                    @if($produit->quantite)
                      <p><strong>{{ number_format($produit->prix, 2, ',', ' ') }} FCFA </strong></p>
                      <form  method="POST" action="{{url('/Panier')}}">
                        @csrf
                        <div class="input-field col">
                          <input type="hidden" id="produit_id" name="produit_id" value="{{ $produit->id }}">
                          <input id="quantite" name="quantite" type="number" value="1" min="1">
                          <label for="quantite">Quantité</label>        
                          <p>
                            <button class="btn waves-effect waves-light" style="width:100%" type="submit" id="addcart">Ajouter au panier
                              <i class="material-icons left">add_shopping_cart</i>
                            </button>
                          <a href="cart.panier" ></a>
                          </p>    
                        </div>    
                      </form>
                      
                    @endif
                  </div>
                </div>
              @endforeach

                </div>


            


               
  <div class='parfum mt-5' >
                <h3 class='para justify-content-start d-flex' style='color:#EF6499' >Les Produits MakeUp</h3>
            </div> --}}
            {{-- <div class="card-group mt-5" id='makeUp'>
                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://storage.googleapis.com/finansialku_media/wordpress_media/2019/08/4b2509db-10-rekomendasi-produk-make-up-lokal-murah-yang-gak-murahan-01-finansialku.jpg' />
                    
                    <h6 class="card-title"> 5000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>


                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://ds393qgzrxwzn.cloudfront.net/resize/m500x500/cat1/img/images/0/mCos6KaAmY.jpg' />
                     
                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>


                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://4.bp.blogspot.com/-XrENeO9sG4U/WRAuyOile-I/AAAAAAAAPM8/gV9wWnIgYaofxtkXTnrqhunrqbpS_dJ_QCLcB/s1600/DSC_4691.JPG' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>

                <div class="card m-3" style='border:none'>
                    <img class="card-img-top" src='https://diva.my/wp-content/uploads/2020/10/collage-6-30.jpg' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>
                </div> --}}

            </div>

        </div>
        
        </div>
    </div>
    <div class="container mt-5" style='background:#f183ad'>
            <footer class="row row-cols-1 row-cols-sm-2 row-cols-md-5 py-5 my-5 border-top">
                <div class="col mb-3">
                    <a href="/" class="d-flex align-items-center mb-3 link-dark text-decoration-none">
                       <h3 class="font-weight-bolder mb-0" style='color:white'>MyShop</h3>
                    </a>
                    <p class="date" style='color:white'>© 2023</p>
                </div>

                <div class="col mb-3">

                </div>

                <div class="col mb-3">

                    <ul class="nav flex-column">
                      <li class="nav-item mb-2"><h3  class="nav-link p-0 " style='color:white; font-size:20px'>Home</h3></li>
                      
                        {{-- <li class="nav-item mb-2"><h3 class="nav-link p-0 style='color:white'>Home</h3></li> --}}
                        {{-- <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Contact</a></li> --}}
                    </ul>
                </div>

                <div class="col mb-3">

                    {{-- <ul class="nav flex-column"> --}}
                        {{-- <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Home</a></li> --}}
                        <h3  class="nav-link p-0 " style='color:white;font-size:20px'>Contact</h3>
                        <i class="fa fa-facebook-official m-3" aria-hidden="true" style="font-size:30px; color:white"></i>
                        <i class="fa fa-whatsapp m-3" aria-hidden="true" style="font-size:30px; color:white"></i>
                        
                        <i class="fa fa-twitter" aria-hidden="true" style="font-size:30px; color:white"></i>
                        {{-- <i class="fa fa-twitter-square round-circle" aria-hidden="true" style="font-size:30px; color:white"></i> --}}
                        {{-- </ul> --}}
                </div>

                {{-- <div class="col mb-3">

                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Contact</a></li>
                    </ul>
                </div> --}}
            </footer>
    </div>

@endsection
