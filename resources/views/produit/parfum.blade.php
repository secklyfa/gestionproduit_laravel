@extends('layouts.app1')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="container ">
      <h3>PARFUMS</h3>
              <div class="row  row-cols-md-4 g-2  justifi-content-center d-flex" '>
              
                @foreach($produits as $produit)
                    <div class="card shadow-flex">
                      <div class="card-image">
                        
                          <img src="assets/img/{{ $produit->image }}" class="produits w-100">
                       
                      </div>          
                      <div class="card-content center-align">
                        <h6>{{ $produit->nomProduit }}</h6>
                        @if($produit->quantite)
                          <h5 ><strong>{{ number_format($produit->prix) }} FCFA </strong></h5>
                          
                         
                        @endif
                      </div>
                    </div>
               
                  @endforeach
               
              </div>
    </div>
              {{-- @foreach($categories as $categorie)
          
              @if($categorie->nom)
                <a href="{{url('/categorie',$categorie->category_id)}}" style="text-decoration: none; text-align:center">
                    <p style="margin:15px" >{{$categorie->nom}}</hp>
                   
                </a>  
              @endif
              
        @endforeach --}}
  
     

@endsection
