@extends('layouts.app')
@section('content')
    <div class="container ">
        <div class="row align-items-center mt-3">
            
            
            <div >
                <h3 class='para justify-content-center d-flex' style='color:#EF6499' >Tous les Categories</h3>
            </div>
            
             
            <div class="card-group mt-5 ">
                @foreach($categories as $categorie)
                
                    @if($categorie->nom)
                      <a href="#parfum">
                          <h4>{{$categorie->nom}}</h4>
                          
                    @endif
                      <img class='image w-100' src="https://media.istockphoto.com/id/1194825741/fr/photo/fermez-vous-vers-le-haut-de-la-bouteille-en-verre-de-parfum-et-bo%C3%AEte-ouverte-de-cadeau-comme.jpg?s=612x612&w=0&k=20&c=GThGie9zisb4jC9NNizH9S0b-5iCNYIDLkPySgOxIAA={{ $categorie->image }}">
                   
              @endforeach

            
                     
                  </div>
              
                         
                {{-- <div class="card m-4 text-align-center" style='background-color:mistyRose; margin-top:0px'>
                  <a href="#parfum">
                    <h4>Parfum</h4>
                    <img class="card-img-top"
                        src='https://parfumsfactory.com/wp-content/uploads/2016/05/2015-05-07-19-51-50banner1.jpg'
                        alt="Card image cap" />
                      
                </a>
                </div> --}}
                



                {{-- <div class="card m-4" style='background-color:mistyRose; margin:30px'>
                <a href="#lait_de_corps">
               
                    <h4 class="card-title">LAIT DE CORPS</h4>
                    <img class="card-img-top" src='https://i.ytimg.com/vi/_TldF-VrbrM/mqdefault.jpg' alt="Card image cap" />
                </a>
                </div>

                <div class="card m-4" style='background-color:mistyRose; margin:30px'>
                    <a href="#makeUp">
                    <h4 class="card-title">MAKE UP</h4>
                    <img class="card-img-top" src='https://communication.binus.ac.id/files/2022/02/34.png'
                        alt="Card image cap" />
                        </a>
                </div> --}}

            </div>
        </div>
         <h3 class='para justify-content-center d-flex mt-5' style='color:#EF6499' >Tous les Produits</h3>
       
         <div class='parfum mt-5' >
                <h3 class='para justify-content-start d-flex' style='color:#EF6499' >Les Parfums</h3>
            </div>
        <div class="card-group mt-5" id='parfum'>
            {{-- <div class="row">
                <div class="col-12 cards-container"> --}}
                  @foreach($produits as $produit)
                    <div class="card m-3">
                      <div class="card-image">
                        @if($produit->quantite)
                          <a href="#">
                        @endif
                          <img src="/images/thumbs/{{ $produit->image }}">
                        @if($produit->quantite) </a> @endif
                      </div>          
                      <div class="card-content center-align">
                        <p>{{ $produit->nomProduit }}</p>
                        @if($produit->quantite)
                          <p><strong>{{ number_format($produit->prix, 2, ',', ' ') }} FCFA </strong></p>
                          <form  method="POST" action="{{route('cart.panier')}}">
                            @csrf
                            <div class="input-field col">
                              <input type="hidden" id="id" name="id" value="{{ $produit->id }}">
                              <input id="quantite" name="quantite" type="number" value="1" min="1">
                              <label for="quantite">Quantité</label>        
                              <p>
                                <button class="btn waves-effect waves-light" style="width:100%" type="submit" id="addcart">Ajouter au panier
                                  <i class="material-icons left">add_shopping_cart</i>
                                </button>
                              </p>    
                            </div>    
                          </form>
                          {{-- @else
                          <p class="red-text"><strong>Produit en rupture de stock</strong></p> --}}
                        @endif
                      </div>
                    </div>
                  @endforeach
                 
                </div>
                {{-- </div>
              </div> --}}
            

            {{-- <div class="card m-3" style='border:none'>
                <img class="card-img-top"
                    src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRD_-9mmnw99OHTLcslH27BBeFuTKv3jddEeShKTFAXQYF3VsbDPJp73hu2ZeKU25QA-uI&usqp=CAU' />

                <h6 class="card-title"> 10000 FR</h6>
                <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                    <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                    <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                        style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                        Ajouter au panier</button>
                </form>

                

            </div> --}}
            {{-- <div class="card m-3" style='border:none'>
                <img class="card-img-top"
                    src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBRUeW5M6QdTlvS92OtC17-MxFD9Kb_m5JKg&usqp=CAU' />

                <h6 class="card-title"> 10000 FR</h6>
                <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                    <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                    <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                        style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                        Ajouter au panier</button>
                </form>


            </div>
            <div class="card m-3" style='border:none'>
                <img class="card-img-top"
                    src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiBxYqebwOPp3DcSaiQfr8OHBaONSqP59EYQ&usqp=CAU' />

                <h6 class="card-title"> 10000 FR</h6>
                <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                    <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                    <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                        style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                        Ajouter au panier</button>
                </form>


            </div>

            <div class="card m-3" style='border:none'>
                <img class="card-img-top"
                    src='http://cdn.shopify.com/s/files/1/1145/8640/products/miss_dior_eau_de_parfum_1200x1200.jpg?v=1591551091' />

                <h6 class="card-title"> 10000 FR</h6>
                <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                    <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                    <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                        style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                        Ajouter au panier</button>
                </form>

            </div>
</div>
  --}}
         {{-- <div>
                <h3 class='para justify-content-start d-flex mt-5' style='color:#EF6499'>Les Lait De Corps</h3>
            </div> --}}
            <div class="card-group mt-5" id='lait_de_corps' >
                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://sunushopping.com/wp-content/uploads/2021/06/YVES-ROCHER-LAIT-HAMMAM.jpg' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3 ">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>


                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ03u7TQpHtiavYo3QMbD2AwJZe6PkgKmeCFw&usqp=CAU' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>


                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxhzpfA5jHE2cXBWaCYSZR0566aN-LMDGS4T9ymmTPEBWjLW0CLW2GmKgeXzSMI5ovrR8&usqp=CAU' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-5"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>

                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='http://cdn.shopify.com/s/files/1/0561/1762/5029/products/Comptoir-Des-Huiles-Lait-Corps-1.jpg?v=1668349373' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>
                </div>

            </div>
  <div class='parfum mt-5' >
                <h3 class='para justify-content-start d-flex' style='color:#EF6499' >Les Produits MakeUp</h3>
            </div>
            <div class="card-group mt-5" id='makeUp'>
                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://storage.googleapis.com/finansialku_media/wordpress_media/2019/08/4b2509db-10-rekomendasi-produk-make-up-lokal-murah-yang-gak-murahan-01-finansialku.jpg' />
                    
                    <h6 class="card-title"> 5000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>


                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://ds393qgzrxwzn.cloudfront.net/resize/m500x500/cat1/img/images/0/mCos6KaAmY.jpg' />
                     
                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>


                <div class="card m-3" style='border:none'>
                    <img class="card-img-top"
                        src='https://4.bp.blogspot.com/-XrENeO9sG4U/WRAuyOile-I/AAAAAAAAPM8/gV9wWnIgYaofxtkXTnrqhunrqbpS_dJ_QCLcB/s1600/DSC_4691.JPG' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>


                </div>

                <div class="card m-3" style='border:none'>
                    <img class="card-img-top" src='https://diva.my/wp-content/uploads/2020/10/collage-6-30.jpg' />

                    <h6 class="card-title"> 10000 FR</h6>
                    <form method="POST" action="#" class="form-inline d-inline-block mt-3">

                        <input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2">
                        <button type="submit" class="bg-gradient-primary shadow-primary border-radius-lg mt-3"
                            style='display: block;
    position: relative;
    margin-left: auto;
    margin-right: auto; 
	color:white;border:none'>+
                            Ajouter au panier</button>
                    </form>
                </div>

            </div>

        </div>
        
        </div>
    </div>
    <div class="container mt-5" style='background:pink'>
            <footer class="row row-cols-1 row-cols-sm-2 row-cols-md-5 py-5 my-5 border-top">
                <div class="col mb-3">
                    <a href="/" class="d-flex align-items-center mb-3 link-dark text-decoration-none">
                       <h3 class="font-weight-bolder mb-0" style='color:white'>MyShop</h3>
                    </a>
                    <p class="text-muted">© 2023</p>
                </div>

                <div class="col mb-3">

                </div>

                <div class="col mb-3">

                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Contact</a></li>
                    </ul>
                </div>

                <div class="col mb-3">

                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Contact</a></li>
                    </ul>
                </div>

                <div class="col mb-3">

                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted" style='color:white'>Contact</a></li>
                    </ul>
                </div>
            </footer>
    </div>

@endsection
