@extends('layouts.appp')

<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    * {
        box-sizing: border-box;
    }

    .input-container {
        display: -ms-flexbox;
        /* IE10 */
        display: flex;
        width: 100%;
        margin-bottom: 15px;
    }

    .icon {
        padding: 10px;
        background: dodgerblue;
        color: white;
        min-width: 50px;
        text-align: center;
        height: 50px;
    }

    .input-field {
        width: 100%;
        padding: 10px;
        outline: none;
    }

    .input-field:focus {
        border: 2px solid dodgerblue;
    }

    /* Set a style for the submit button */
    .btn {
        background-color: dodgerblue;
        color: white;
        padding: 15px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    .btn:hover {
        opacity: 1;
    }
</style>



<div class='bg-gray-200'>
    @section('content')
        <div class="container-fluid m-0 p-0  mt-5" style='background-color:'>
            <div class="row justify-content-center m-0 p-0">
                <div class="col-md-12 ">

                    <div class="row">
                        <div class="col-md-7 d-flex justify-content-center align-items-center mt-5 h-50">
                            <img src="https://cdn-icons-png.flaticon.com/512/6867/6867595.png"
                                class="shadow-lg p-5 mb-5  rounded-circle w-40" alt="Sample image" style='border-radius:50%'>
                        </div>

                        <div class="col-6 col-md-5 ">
                            <div class="container ">
                                <div class="row justify-content-star mt-3">
                                    <div class="col-md-8">

                                        <h3 class='title text-align:center mb-5' style='padding-left:80px'>{{ __('CONNEXION') }}
                                        </h3>
                                        <form method="POST" action="{{ route('login') }}">

                                            @csrf

                                            <div class="col-md-12 mt-3">
                                                <input id="email" type="email" placeholder='Adresse Email'
                                                    class="form-control @error('email') is-invalid @enderror shadow-lg p-3 mb-5"
                                                    name="email" value="{{ old('email') }}" style='border-radius:50px'
                                                    required autocomplete="email" autofocus>

                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>




                                            <div class="col-md-12">
                                                <input id="password" type="password" placeholder='Mot de passe'
                                                    class="form-control @error('password') is-invalid @enderror shadow-lg p-3 mb-3"
                                                    name="password" style='border-radius:50px' required
                                                    autocomplete="current-password">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>


                                            <div class="row mb-5">
                                                <div class="col-md-12 offset-md-4">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="remember"
                                                            id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label" for="remember">
                                                            {{ __('Souviens toi de moi') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <a class="btn btn-link " href="{{ route('projet1') }}">
                                                        <button type="submit" class="btn btn-primary shadow-lg p-3 mb-5"
                                                            style='border-radius:50px; background:#63C9E8'>
                                                            {{ __('Se Connecté') }}
                                                        </button>
                                                    </a>
                                                    @if (Route::has('registration'))
                                                        <a class="btn btn-link" href="{{ route('registration') }}">
                                                            {{ __('INSCRIRE') }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endsection
</div>
