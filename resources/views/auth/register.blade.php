
@extends('layouts.appp')
<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
        
       
         
    }

    * {
        box-sizing: border-box;
    }

    .input-container {
        display: -ms-flexbox;
        /* IE10 */
        display: flex;
        width: 100%;
        margin-bottom: 15px;
    }

    .icon {
        padding: 10px;
        background: dodgerblue;
        color: white;
        min-width: 50px;
        text-align: center;
        height:50px;
    }

    .input-field {
        width: 100%;
        padding: 10px;
        outline: none;
    }

    .input-field:focus {
        border: 2px solid dodgerblue;
    }

    /* Set a style for the submit button */
    .btn {
        background-color: dodgerblue;
        color: white;
        padding: 15px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    .btn:hover {
        opacity: 1;
    }
</style>
<div class='bg-gray-200'>
@section('content')
    <div class="container-fluid m-0 p-0" '>
            <div class="row justify-content-center m-0 p-0">
                <div class="col-md-12 ">
                       
                         <div class="row mt-5">
                           <div class="col-md-7 d-flex justify-content-center align-items-center mt-5  h-50 ">
                             <img src="https://records.fullerton.edu/_resources/images/icons/landing-page/registration.png"
                            class="shadow-lg p-3 mt-5  rounded-circle " alt="Sample image" style='border-radius:50%; width:300px ;margin-top:30px'; >
                           </div>

     <div class="col-6 col-md-5">
        <div class="container-fluid m-0 p-0">
            <div class="row justify-content-star">
                <div class="col-md-8">
                <div class='title justify-content-star '>
                 <h3 class='titl  justify-content-star ' style='padding-left:80px'>{{ __('INSCRIPTION') }}</h3>
                </div>
                   
                   <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                           

                            <div class="col-md-12 mt-5">
                                <input id="name" style='border-radius:50px' type="text" placeholder='Nom' class="form-control @error('name') is-invalid @enderror shadow-lg p-3 mb-3  " name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                           

                            <div class="col-md-12">
                                <input id="email" style='border-radius:50px' placeholder='Adresse Email'  type="email" class="form-control @error('email') is-invalid @enderror shadow-lg p-3 mb-3" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                          
                            <div class="col-md-12">
                                <input id="password" style='border-radius:50px' type="password" placeholder='Mot de Passe' class="form-control @error('password') is-invalid @enderror shadow-lg p-3 mb-3" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                          

                            <div class="col-md-12">
                                <input id="password-confirm" style='border-radius:50px' type="password" placeholder='Confirmer' class="form-control shadow-lg p-3 mb-3" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary shadow-lg p-3 mb-5" style='border-radius:50px; background:#63C9E8'>
                                    {{ __('INSCRIRE') }}
                                </button>
                            </div>
                        </div>
                    </form>     
                        
    </div>
    </div>
    </div>
    </div>
@endsection
</div>
